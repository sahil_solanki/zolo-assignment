import React from 'react'
import { NavLink } from 'react-router-dom'

import styles from './Menu.css'
import classNames from 'classnames/bind'

let cx = classNames.bind(styles)

const Menu = ({children, isOpen, handleClick, logout}) => {
  let menuClass = cx({
    'dropdown-menu': true,
    dropdownMenu: true,
    open: isOpen
  })
  return (
    <ul className={menuClass} onClick={handleClick}>
      <li className={styles.listItem}><NavLink exact activeClassName={styles.active} className={styles.link} to='/'>Home</NavLink></li>
      <li className={styles.listItem}><a className={styles.link} onClick={logout} href>Logout</a></li>
      <li className={styles.listItem}><a className={styles.link} href='https://bitbucket.org/sahil_solanki/zolo-assignment' target='_blank'>Source Code</a></li>
    </ul>
  )
}

export default Menu
