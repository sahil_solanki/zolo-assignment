import React from 'react'

import { NavLink, withRouter } from 'react-router-dom'

import MobileMenu from './MobileMenu/MobileMenu'
import '../../base.css'
import styles from './Header.css'

class Header extends React.Component {
  constructor (props) {
    super(props)
    this.logout = this.logout.bind(this)
  }

  logout () {
    localStorage.removeItem('userEmail')
    localStorage.removeItem('userPassword')
    setTimeout(() => {
      this.props.history.push('/login')
    }, 300)
  }

  render () {
    return (
      <div>
        {localStorage.getItem('userEmail') != null &&
          <header className={styles.header}>
            <div className={styles.container}>
              <h1 className={styles.title}>Zolo Assignment</h1>
              <nav className={styles.desktopNav}>
                <ul className={styles.list}>
                  <li className={styles.listItem}><NavLink exact activeClassName={styles.active} className={styles.link} to='/'>Home</NavLink></li>
                  <li className={styles.listItem}><a className={styles.link} onClick={this.logout}>Logout</a></li>
                  <li className={styles.listItem}><a className={styles.link} href='https://bitbucket.org/sahil_solanki/zolo-assignment' target='_blank'>Source Code</a></li>
                </ul>
              </nav>
              <MobileMenu logout={this.logout} />
            </div>
          </header>
        }
      </div>
    )
  }
}

export default withRouter(Header)
