import React from 'react'

import styles from './QuestionCard.css'

const QuestionCard = ({ques, quesNo, handleSubmit, show, isCorrect, isSubmitted}) => {
  const optionsList = []
  ques.options.map( (option, key) => {
    optionsList.push(<li className={styles.option} key={key}> <button className={styles.button} onClick={(e) => handleSubmit(quesNo, key)}> {option} </button> </li>)
  })

  return (
    <div>
      {show &&
        <div>
          <p className={styles.question}>
            Q{quesNo}. {ques.text}
          </p>
          <ul className={styles.optionsList}> {optionsList} </ul>
          {isSubmitted && isCorrect &&
            <p className="text-success small"> Correct Answer! </p>
          }
          {isSubmitted && !isCorrect &&
            <p className="text-danger small"> Wrong Answer! </p>
          }
        </div>
      }
    </div>
  )
}

export default QuestionCard
