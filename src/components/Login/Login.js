import React from 'react'

import {withRouter} from 'react-router-dom'
import FormControl from 'react-bootstrap/lib/FormControl'
import Button from 'react-bootstrap/lib/Button'

import avatarUrl from '../../images/avatar.png'
import styles from './Login.css'
import authUser from 'api/auth'

class Login extends React.Component {
  constructor () {
    super()
    this.state = {
      'email': '',
      'password': '',
      'isError': '',
    }
    this.loginUser = this.loginUser.bind(this)
    this.handleEmailChange = this.handleEmailChange.bind(this)
    this.handlePasswordChange = this.handlePasswordChange.bind(this)
  }

  componentWillMount () {
    let isLoggedIn = localStorage.getItem('userEmail')
    if (isLoggedIn){
      this.props.history.push('/')
      return false
    }
  }

  handleEmailChange (e) {
    this.setState({email: e.target.value, isError: ''});
  }

  handlePasswordChange (e) {
    this.setState({password: e.target.value, isError: ''});
  }

  loginUser (e) {
    e.preventDefault()
    let resp = authUser(this.state.email, this.state.password)
    if(resp.loginSuccess){
      localStorage.setItem('userEmail', this.state.name)
      localStorage.setItem('userPassword', this.state.password)
      this.setState({email:'', password:'', isError: ''})
      this.props.history.push('/')
    }else {
      this.setState({email:'', password:'',isError: 'username or password incorrect'})
    }
  }

  render () {
    return (
      <div className={styles.loginContainer + ' container'}>
        <div className={styles.card}>
            <div className={styles.avatarContainer}>
              <img id="profileImg" className={styles.profileImg} src={avatarUrl} />
            </div>
            <form className={styles.formSignin} onSubmit={this.loginUser}>
              <FormControl
                id="userEmail"
                type="email"
                label="Email address"
                placeholder="Enter Email"
                value={this.state.email}
                onChange={this.handleEmailChange}
                autoComplete="off"
              />
              <FormControl
                id="userPassword"
                label="Password"
                type="password"
                placeholder="Enter Password"
                value={this.state.password}
                onChange={this.handlePasswordChange}
                autoComplete="off"
              />
              {this.state.isError !== '' &&
                <p className="text-danger text-small"> {this.state.isError}</p>
              }
              <Button type="submit" bsSize="large" bsStyle="primary" className={styles.btnLogin} block >
                Sign in
              </Button>
            </form>
        </div>
      </div>
    )
  }
}

export default withRouter(Login)
