import React from 'react'

import questionData from 'data/questions.json'
import {withRouter} from 'react-router-dom'

import Chart from 'chart.js'

import styles from './Summary.css'
import QuestionSummary from './QuestionSummary/QuestionSummary'

class Summary extends React.Component {
  constructor () {
    super()
    this.state = {
      'userResp': [],
      'respResults': []
    }
    this.resetTest = this.resetTest.bind(this)
  }
  componentWillMount () {
    const userResp = JSON.parse(localStorage.getItem('userResp'))
    const respResults = JSON.parse(localStorage.getItem('results'))
    if(!localStorage.getItem('userResp')){
      this.props.history.push('/')
      return false
    }
    this.setState({email: localStorage.getItem('userEmail'), userResp, respResults})
  }

  componentDidMount () {
    let graph = document.getElementById('chart')
    if(!graph) return false
    let respResults = this.state.respResults
    let correctAnsCount = 0, wrongAnsCount = 0
    if(respResults.length){
      correctAnsCount = respResults.reduce( (count, val) => {
        return count + (val === true)
      }, 0)
      wrongAnsCount = respResults.reduce( (count, val) => {
        return count + (val === false)
      }, 0)
    }
    const unAnsweredCount = questionData.questions.length - correctAnsCount - wrongAnsCount

    const data = {
      datasets: [{
        data: [correctAnsCount, wrongAnsCount, unAnsweredCount],
        backgroundColor: ['#5cb85c', '#d9534f', '#337ab7']
      }],
      labels: ['Correct', 'Wrong', 'Un-answered']
    };

    let ctx = graph.getContext("2d")
    const chart = new Chart(ctx, {
			type: 'pie',
			data: data,
		})
  }

  resetTest () {
    localStorage.removeItem('userResp')
    localStorage.removeItem('results')
    this.props.history.push('/')
  }

  render () {
    const questionsList = []
    const questions = questionData.questions
    questions.map( (ques, key) => {
      const correctRespIndex = ques['correctRespIndex']
      const correctResp = ques['options'][correctRespIndex]
      const userResp = ques['options'][this.state.userResp[key]]
      questionsList.push(<QuestionSummary ques = {ques} quesNo={key + 1} key={key} correctResp={correctResp} userResp={userResp} isCorrect={this.state.respResults[key] === true} isSubmitted={this.state.respResults[key] !== undefined} />)
    })

    return (
      <div className={"col-md-8 col-md-offset-2 " + styles.container}>
        <div className="row">
          <div className="col-md-6">
            {questionsList}
          </div>
          <div className="col-md-6">
            <div className={styles.graph}>
              <canvas id='chart'/>
            </div>
          </div>
        </div>
        <div className="col-md-12 text-center">
          <button className={"btn btn-success btn-lg " + styles.resetBtn} onClick={this.resetTest}> Restart Test </button>
        </div>
      </div>
    )
  }
}

export default withRouter(Summary)
