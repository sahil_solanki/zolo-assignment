import React from 'react'

import styles from './QuestionSummary.css'
import romanNumberMap from 'data/romanNumberMap.json'

const QuestionSummary = ({ques, quesNo, correctResp, userResp, isCorrect, isSubmitted}) => {

  return (
    <div className={styles.question}>
      <p>
        Q{quesNo}. {ques.text}
      </p>
      <p className="text">
        Correct Answer: <b> {correctResp} </b>
      </p>
      <p className="text">
        <i> Your Response: <b> {userResp} </b> </i>
        {isCorrect && isSubmitted &&
          <span className={"label label-success "+ styles.label}>Correct Answer</span>
        }
        {!isCorrect && isSubmitted &&
          <span className={"label label-danger " + styles.label}>Wrong Answer</span>
        }
        {!isSubmitted &&
          <span className={"label label-primary " + styles.label}>Un-Answered</span>
        }
      </p>
      <hr/>
    </div>
  )
}

export default QuestionSummary
