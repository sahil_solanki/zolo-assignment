import React from 'react'
import { Switch, Route } from 'react-router-dom'

import AsyncRoute from '../AsyncRoute/AsyncRoute'

const Routes = () => (
  <div>
    <Switch>
      <Route path='/login' component={props => <AsyncRoute loading={System.import('../Login/Login')} />} />
      <Route exact path='/' component={props => <AsyncRoute loading={System.import('../Home/Home')} />} />
      <Route path='/summary' component={props => <AsyncRoute loading={System.import('../Summary/Summary')} />} />
      <Route component={props => <AsyncRoute loading={System.import('../NotFound/NotFound')} />} />
    </Switch>
  </div>
)

export default Routes
