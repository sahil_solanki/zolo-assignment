import React from 'react'
import {withRouter} from 'react-router-dom'

import questionData from 'data/questions.json'

import styles from './Home.css'

import QuestionCard from '../QuestionCard/QuestionCard'

let timer

function formatTime (time) {
  return ('' + time).length === 1 ? ('0' + time) : time
}
class Home extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      currentQues: 1,
      currentTime: 180
    }
    this.handleSubmit = this.handleSubmit.bind(this)
    this.showSummary = this.showSummary.bind(this)
    this.setTimer = this.setTimer.bind(this)
  }

  componentWillMount () {
    let isLoggedIn = localStorage.getItem('userEmail')
    if (!isLoggedIn) {
      this.props.history.push('/login')
      return false
    }

    const responses = JSON.parse(localStorage.getItem('userResp')) || []
    const results = JSON.parse(localStorage.getItem('results')) || []
    this.setState({responses, results})
    if (results.length === 0){
      timer = setInterval(() => this.setTimer() , 1000)
    }
  }

  setTimer () {
    if (this.state.currentTime > 0) {
      this.setState({currentTime: --this.state.currentTime})
    } else {
      clearInterval(timer)
    }
  }

  handleSubmit (quesNo, ansIndex) {
    const questions = questionData.questions
    let nextQues = this.state.currentQues
    let currentResults = this.state.results

    if(questions[quesNo-1]['correctRespIndex'] === ansIndex){
      currentResults[quesNo-1] = true
    }else{
      currentResults[quesNo-1] = false
    }
    this.setState({results: currentResults})
    nextQues++

    setTimeout( () => {
      this.setState({responses: this.state.responses.concat(ansIndex), currentQues: nextQues})
      if(this.state.responses.length === questionData.questions.length){
        this.showSummary()
      }
    }, 1000)
  }

  showSummary () {
    clearInterval(timer)
    localStorage.setItem('userResp', JSON.stringify(this.state.responses))
    localStorage.setItem('results', JSON.stringify(this.state.results))
    this.props.history.push('/summary')
  }

  render () {
    const questionsList = []
    const questions = questionData.questions
    questions.map( (ques, key) => {
      questionsList.push(<QuestionCard ques={ques} quesNo={key + 1} handleSubmit={this.handleSubmit} key={key} show={this.state.currentQues== key+1} isCorrect={this.state.results[key] === true} isSubmitted={this.state.results[key] !== undefined}/>)
    })

    const minutes = Math.floor(this.state.currentTime / 60)
    const seconds = this.state.currentTime - minutes * 60
    const timeIsUp = (minutes === 0 && seconds === 0)
    const allAnswered = this.state.responses.length === questionData.questions.length
    return (
      <div className={"col-md-8 col-md-offset-2 " + styles.container}>
          {!allAnswered  && !timeIsUp &&
            <div>
              <div className={"col-xs-9 " + styles.questionsList}>
               {questionsList}
              </div>
              <div className={"col-xs-3 " + styles.timerCard}>
                <p> Remaining Time- <span className="text-success"> {formatTime(minutes)}:{formatTime(seconds)} </span> </p>
              </div>
            </div>
          }
          <div className="row">
            {timeIsUp &&
              <div className={"col-xs-6 col-xs-offset-3 " + styles.timerCard}>
                <p className="text-danger"> Times'up </p>
              </div>
            }
            {allAnswered &&
              <div className={"col-xs-6 col-xs-offset-3 " + styles.timerCard}>
                <p className="text-success"> All questions answered </p>
              </div>
            }
          </div>
          {(timeIsUp || allAnswered) &&
            <div className="col-md-12 text-center">
              <button className={"btn btn-primary btn-lg " + styles.summaryBtn} onClick={this.showSummary}> Show Summmary </button>
            </div>
          }
      </div>
    )
  }
}

export default withRouter(Home)
