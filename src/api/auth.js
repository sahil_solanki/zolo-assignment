import usersData from 'data/user.json'

function authUser (email, password) {
  let userFound = false
  usersData.users.forEach((user, key) => {
    if (user.email === email && user.password === password) {
      userFound = true
    }
  })
  return {loginSuccess: userFound}
}

export default authUser
